$(window).on("load", function() {
  // Page loader
  $("body").addClass("load-complete");
  $(".page-loader img").fadeOut();
  $(".page-loader")
    .delay(200)
    .fadeOut("slow");

  $("body").imagesLoaded(function() {
    setTimeout(function(e) {}, 0);
  });
});

var htmlElement = document.documentElement;
var bodyElement = document.body;
var pageOverlay = document.getElementById("overlay");

var FOODY = {
  // _API: "https://publicapi.vienthonga.vn",
  // _URL: "https://vienthonga.vn"
};

var Layout = (function() {
  var setUserAgent = function() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      bodyElement.classList.add("window-phone");
      return;
    }

    if (/android/i.test(userAgent)) {
      bodyElement.classList.add("android");
      return;
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      bodyElement.classList.add("ios");
      return;
    }
  };

  var viewportHeight = function() {
    // First we get the viewport height and we multiple it by 1% to get a value for a vh unit
    let vh = window.innerHeight * 0.01;
    // Then we set the value in the --vh custom property to the root of the document
    document.documentElement.style.setProperty("--vh", `${vh}px`);

    // We listen to the resize event
    window.addEventListener("resize", () => {
      // We execute the same script as before
      let vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty("--vh", `${vh}px`);
    });
  };

  var loadingPage = function() {};

  var scrollToTop = function() {
    var scrollTop = $("#scrollToTop");

    $(window).on("scroll", function() {
      if ($(window).scrollTop() > 200) {
        scrollTop.addClass("active");
      } else {
        scrollTop.removeClass("active");
      }
    });

    scrollTop.click(function() {
      $("body, html").animate(
        {
          scrollTop: 0
        },
        500
      );
    });
  };

  return {
    init: function() {
      setUserAgent();
      scrollToTop();
      loadingPage();
    }
  };
})();
var myFullpage;
$(document).ready(function() {
  Layout.init();

  myFullpage = new fullpage("#fullpage", {
    navigation: false,
    scrollingSpeed: 400,
    menu: ".my-menu",
    scrollOverflow: true,
    slidesNavigation: false,
    normalScrollElements: ".htu-nav",
    responsiveWidth: 700
  });

  $(".my-menu li").on("click", function() {
    $(".navbar-collapse").collapse("hide");
  });

  //set scrolling variables
});

var scrolling = false,
  previousTop = 0,
  currentTop = 0,
  scrollDelta = 10,
  scrollOffset = 100,
  scrollNav = 200;

function autoHideHeader() {
  var currentTop = $(window).scrollTop();

  if (currentTop >= scrollNav) {
    bodyElement.classList.add("is-scroll-nav");
  } else {
    bodyElement.classList.remove("is-scroll-nav");
  }

  if (previousTop - currentTop > scrollDelta) {
    //if scrolling up...
    bodyElement.classList.remove("is-scroll-down");
  } else if (
    currentTop - previousTop > scrollDelta &&
    currentTop > scrollOffset
  ) {
    //if scrolling down...
    bodyElement.classList.add("is-scroll-down");
  }

  previousTop = currentTop;
  scrolling = false;
}

autoHideHeader();

$(window).on("scroll", function() {
  if (!scrolling) {
    scrolling = true;
    !window.requestAnimationFrame
      ? setTimeout(autoHideHeader, 250)
      : requestAnimationFrame(autoHideHeader);
  }
});

{
  //Calculates the offsetTop or offsetLeft of an element relative to the viewport
  // (not counting with any transforms the element might have)
  const getOffset = (elem, axis) => {
    let offset = 0;
    const type = axis === "top" ? "offsetTop" : "offsetLeft";
    do {
      if (!isNaN(elem[type])) {
        offset += elem[type];
      }
    } while ((elem = elem.offsetParent));
    return offset;
  };

  //Calculates the distance between two points
  const distance = (p1, p2) => Math.hypot(p2.x - p1.x, p2.y - p1.y);
  //Generates a random number.
  const randNumber = (min, max) =>
    Math.floor(Math.random() * (max - min + 1)) + min;

  //Gets the mouse position
  const getMousePos = e => {
    let posx = 0;
    let posy = 0;
    if (!e) e = window.event;
    if (e.pageX || e.pageY) {
      posx = e.pageX;
      posy = e.pageY;
    } else if (e.clientX || e.clientY) {
      posx =
        e.clientX +
        document.body.scrollLeft +
        document.documentElement.scrollLeft;
      posy =
        e.clientY +
        document.body.scrollTop +
        document.documentElement.scrollTop;
    }
    return {
      x: posx,
      y: posy
    };
  };

  // Returns the rotation angle of an element.
  const getAngle = el => {
    const st = window.getComputedStyle(el, null);
    const tr = st.getPropertyValue("transform");
    let values = tr.split("(")[1];
    values = values.split(")")[0];
    values = values.split(",");
    return Math.round(Math.asin(values[1]) * (180 / Math.PI));
  };

  // Scroll control functions. Taken from https://stackoverflow.com/a/4770179.
  const keys = {
    37: 1,
    38: 1,
    39: 1,
    40: 1
  };
  const preventDefault = e => {
    e = e || window.event;
    if (e.preventDefault) e.preventDefault();
    e.returnValue = false;
  };
  const preventDefaultForScrollKeys = e => {
    if (keys[e.keyCode]) {
      preventDefault(e);
      return false;
    }
  };
  const disableScroll = () => {
    if (window.addEventListener)
      // older FF
      window.addEventListener("DOMMouseScroll", preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove = preventDefault; // mobile
    document.onkeydown = preventDefaultForScrollKeys;
  };
  const enableScroll = () => {
    if (window.removeEventListener)
      window.removeEventListener("DOMMouseScroll", preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
  };

  class ParallaxItem {
    constructor(el, tiltx = [-1, 1], tilty = [-1, 1]) {
      this.DOM = {
        el: el
      };
      this.config = {
        x: tiltx,
        y: tilty
      };
      this.initEvents();
      // console.log(el);
    }

    initEvents() {
      this.mousemoveFn = ev =>
        requestAnimationFrame(() => {
          this.tilt(ev);
        });

      window.addEventListener("mousemove", this.mousemoveFn);
    }

    tilt(ev) {
      //Get mouse position
      const mousepos = getMousePos(ev);
      // Document scrolls.
      const docScrolls = {
        left: body.scrollLeft + docEl.scrollLeft,
        top: body.scrollTop + docEl.scrollTop
      };
      const bounds = this.DOM.el.getBoundingClientRect();
      // Mouse position relative to the main element (this.DOM.el).
      const relmousepos = {
        x: mousepos.x - bounds.left - docScrolls.left,
        y: mousepos.y - bounds.top - docScrolls.top
      };

      let t = this.config;
      //Movement settings for the tilt elements.
      TweenMax.to(this.DOM.el, 2, {
        ease: Expo.easeOut,
        x: ((t.x[1] - t.x[0]) / bounds.width) * relmousepos.x + t.x[0],
        y: ((t.y[1] - t.y[0]) / bounds.height) * relmousepos.y + t.y[0]
      });
    }
  }

  class HouseScale {
    constructor(el) {
      this.DOM = {
        el: el
      };

      this.DOM.label = this.DOM.el.querySelector(".house-label");

      this.DOM.img = this.DOM.el.querySelector(".house-img");

      this.initEvents();
    }

    initEvents() {
      this.toggleAnimationOnHover = type => {
        // Scale up the img element.
        TweenMax.to(this.DOM.img, 1, {
          ease: Expo.easeOut,
          scale: type === "mouseenter" ? 1.1 : 1
        });

        TweenMax.to(this.DOM.label, 1, {
          ease: Expo.easeOut,
          autoAlpha: type === "mouseenter" ? 1 : 0,
          y: type === "mouseenter" ? -30 : 0
        });
      };

      this.toggleAnimationRandom = () => {
        function getRandom(min, max) {
          return min + Math.random() * (max - min);
        }
        var delay = getRandom(0, 10);

        // console.log(delay);

        var tl = new TimelineMax({
          repeatDelay: 9,
          repeat: -1,
          delay: delay
        });

        var tl1 = new TimelineMax({
          repeatDelay: 9,
          repeat: -1,
          delay: delay
        });

        tl.to(this.DOM.img, 1, {
          ease: Expo.easeOut,
          scale: 1.1
        }).to(
          this.DOM.img,
          1,
          {
            ease: Expo.easeOut,
            scale: 1
          },
          3
        );

        tl1
          .to(this.DOM.label, 1, {
            ease: Expo.easeOut,
            autoAlpha: 1,
            y: -30
          })
          .to(
            this.DOM.label,
            1,
            {
              ease: Expo.easeOut,
              autoAlpha: 0,
              y: 30
            },
            3
          );
      };

      this.mouseenterFn = ev => {
        this.toggleAnimationOnHover(ev.type);
      };

      this.mouseleaveFn = ev => {
        this.resetScale();
        this.toggleAnimationOnHover(ev.type);
      };

      this.DOM.el
        .querySelector(".house-img")
        .addEventListener("mouseenter", this.mouseenterFn);
      this.DOM.el
        .querySelector(".house-img")
        .addEventListener("mouseleave", this.mouseleaveFn);

      this.toggleAnimationRandom();
    }

    resetScale() {
      TweenMax.to(this.DOM.img, 2, {
        ease: Expo.easeOut,
        scale: 1
      });
    }
  }

  /*   items = [];
    Array.from(document.querySelectorAll('.coin-item')).forEach(
      itemEl => this.items.push(new CoinItem(itemEl))
    );
   */
  class Momo {
    constructor(el) {
      this.DOM = {
        el: el
      };

      if (document.querySelectorAll(".coin-item").length > 0) {
        this.coins = [];
        Array.from(this.DOM.el.querySelectorAll(".coin-item")).forEach(itemEl =>
          this.coins.push(new ParallaxItem(itemEl))
        );

        /* this.heroImage = [];
        Array.from(this.DOM.el.querySelectorAll('.hero-thumbnail')).forEach(
          itemEl => this.coins.push(new ParallaxItem(itemEl, [-5, 5], [-5, 5]))
        ); */

        this.houses = [];
        Array.from(this.DOM.el.querySelectorAll(".house-item")).forEach(
          itemEl =>
            this.houses.push(new ParallaxItem(itemEl, [-0.5, 0.5], [0, 0]))
        );
        this.houseHover = [];
        Array.from(this.DOM.el.querySelectorAll(".house-item")).forEach(
          itemEl => this.houseHover.push(new HouseScale(itemEl))
        );
      }
    }
  }

  // Caching some stuff..
  const body = document.body;
  const docEl = document.documentElement;

  // Window sizes.
  let winsize;
  const calcWinsize = () =>
    (winsize = {
      width: window.innerWidth,
      height: window.innerHeight
    });
  calcWinsize();
  window.addEventListener("resize", calcWinsize);

  // Initialize the Grid.
  // const grid = new Grid(document.querySelector('.grid'));

  // Preload images.
  imagesLoaded(document.querySelectorAll(".grid__item-img"), () => {
    body.classList.remove("loading");
  });

  // Initialize the Momo.
  if (window.innerWidth > 600) {
    const momo = new Momo(document.querySelector(".hero-container"));
  }

  $(".manual-device").each(function(index, element) {
    var $this = $(this);
    var numSlider = $this.attr("childId");
    var device = null;
    var process = null;

    if (numSlider != undefined) {
      device = $this.find(".manual-device-swiper#hd-sub-swp-" + numSlider);
      process = $this
        .parent()
        .parent()
        .find(".manual-process#hd-sub-ctn-" + numSlider);
    } else {
      numSlider = index;
      device = $this.find(".manual-device-swiper");

      process = $this
        .parent()
        .parent()
        .find(".manual-process ");
    }

    var swiperArray = [];

    swiperArray[numSlider] = new Swiper(device, {
      observer: true,
      observeParents: true,
      on: {
        slideChangeTransitionEnd: function slideChangeTransitionEnd() {
          var num = this.activeIndex;
          process.children(".process_item").removeClass("active");
          process
            .children(".process_item")
            .eq(num)
            .addClass("active");
        }
      }
    });

    process.children(".process_item").each(function(index, element) {
      var num = index;
      $(element).on("click", function(e) {
        e.preventDefault();
        $(this)
          .addClass("active")
          .siblings()
          .removeClass("active");
        swiperArray[numSlider].slideTo(index);
      });
    });
  });

  $(".process__body-content").each(function(index, element) {
    var $this = $(this);
    var height = $this[0].scrollHeight;
    var getHeight = height == "0" ? "auto" : height + "px";
    $this.css("--max-height", getHeight);
  });

  $(".promotion-accordion").on("shown.bs.collapse", function() {
    // console.log("dwd");
    fullpage_api.reBuild();
  });
}
